package io.jmix2mvp.petclinic.controller;

import io.jmix2mvp.petclinic.BaseTest;
import io.jmix2mvp.petclinic.dto.PetTypeDTO;
import io.jmix2mvp.petclinic.dto.PetTypeInputDTO;
import io.jmix2mvp.petclinic.entity.PetType;
import io.jmix2mvp.petclinic.mapper.DTOMapper;
import io.jmix2mvp.petclinic.repository.PetTypeRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.graphql.tester.AutoConfigureHttpGraphQlTester;
import org.springframework.graphql.execution.ErrorType;
import org.springframework.graphql.test.tester.GraphQlTester;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.Optional;

import static io.jmix2mvp.petclinic.Authorities.ADMIN;
import static org.assertj.core.api.Assertions.assertThat;

@AutoConfigureHttpGraphQlTester
@WithMockUser(authorities = {ADMIN})
public class PetTypeControllerIT extends BaseTest {
    @Autowired
    private GraphQlTester graphQlTester;

    @Autowired
    private PetTypeRepository petTypeRepository;
    @Autowired
    private DTOMapper dtoMapper;

    @BeforeEach
    void setUp() {
        //remove sample data
        clearAllTables();
    }

    @AfterEach
    void tearDown() {
        //cleanup data
        petTypeRepository.deleteAll();
    }

    @Test
    public void testDeletePetType() {
        //given: initial data
        PetType petType = createPetTypeEntity();
        petTypeRepository.saveAndFlush(petType);

        //when: send a GraphQL mutation to delete PetType
        graphQlTester.documentName("deletePetType")
                .variable("id", petType.getId())
                .executeAndVerify();

        //then: check PetType is deleted
        boolean petTypeExists = petTypeRepository.existsById(petType.getId());
        assertThat(petTypeExists).isFalse();
    }

    @Test
    public void testDeleteNonExistingPetType() {
        //when: send a GraphQL mutation to delete non-existing PetType
        //then: check error in the response
        graphQlTester.documentName("deletePetType")
                .variable("id", Long.MAX_VALUE)
                .execute()
                .errors()
                .satisfy(responseErrors -> {
                    assertThat(responseErrors).hasSize(1);
                    assertThat(responseErrors.get(0).getErrorType()).isEqualTo(ErrorType.INTERNAL_ERROR);
                });
    }

    @Test
    public void testDeleteManyPetType() {
        //given: initial data
        PetType petType = createPetTypeEntity();
        petTypeRepository.saveAndFlush(petType);

        List<Long> ids = List.of(petType.getId());

        //when: send a GraphQL mutation to delete PetTypes
        graphQlTester.documentName("deleteManyPetType")
                .variable("ids", ids)
                .executeAndVerify();

        //then: check PetType is deleted
        boolean petTypeExists = petTypeRepository.existsById(petType.getId());
        assertThat(petTypeExists).isFalse();
    }

    @Test
    public void testGetAllPetTypes() {
        //given: initial data
        PetType petType = createPetTypeEntity();
        petTypeRepository.saveAndFlush(petType);

        //when: send a GraphQL query to get all PetTypes
        //then: check data in the response
        graphQlTester.documentName("petTypeList")
                .execute()
                .path("petTypeList")
                .entityList(PetTypeDTO.class)
                .hasSize(1);
    }

    @Test
    public void testGetPetTypesByIds() {
        //given: initial data
        PetType petType = createPetTypeEntity();
        petTypeRepository.saveAndFlush(petType);

        List<Long> ids = List.of(petType.getId());

        //when: send a GraphQL query to get PetTypes by ids
        //then: check data in the response
        graphQlTester.documentName("petTypeListByIds")
                .variable("ids", ids)
                .execute()
                .path("petTypeListByIds")
                .entityList(PetTypeDTO.class)
                .hasSize(1);
    }

    @Test
    public void testGetPetType() {
        //given: initial data
        PetType petType = createPetTypeEntity();
        petTypeRepository.saveAndFlush(petType);

        //when: send a GraphQL query to get PetType details by id
        //then: check data in the response
        graphQlTester.documentName("petType")
                .variable("id", petType.getId())
                .execute()
                .path("petType")
                .entity(PetTypeDTO.class)
                .satisfies(returnedValue -> {
                    assertThat(returnedValue.getId()).isEqualTo(petType.getId());
                });
    }

    @Test
    public void testGetNonExistingPetType() {
        //when: send a GraphQL query to get details about non-existing PetType
        //then: check error in the response
        graphQlTester.documentName("petType")
                .variable("id", Long.MAX_VALUE)
                .execute()
                .errors()
                .satisfy(responseErrors -> {
                    assertThat(responseErrors).hasSize(1);
                    assertThat(responseErrors.get(0).getErrorType()).isEqualTo(ErrorType.INTERNAL_ERROR);
                });
    }

    @Test
    public void testCreatePetType() {
        //given: initial data
        PetType petType = createPetTypeEntity();
        PetTypeInputDTO input = dtoMapper.petTypeToInputDTO(petType);

        //when: send a GraphQL mutation to create PetType
        PetTypeDTO returnedValue = graphQlTester.documentName("updatePetType")
                .variable("input", input)
                .execute()
                .path("updatePetType")
                .entity(PetTypeDTO.class)
                .get();

        //then: check PetType is created correctly
        Optional<PetType> petTypeOptional = petTypeRepository.findById(returnedValue.getId());
        assertThat(petTypeOptional).isPresent();

        PetType createdPetType = petTypeOptional.get();
        //Check entity attributes
        assertThat(createdPetType.getName()).isEqualTo("name");
    }

    @Test
    public void testUpdatePetType() {
        //given: initial data
        PetType petType = createPetTypeEntity();
        petTypeRepository.saveAndFlush(petType);

        PetTypeInputDTO input = new PetTypeInputDTO();
        input.setId(petType.getId());
        input.setName("updatedName");

        //when: send a GraphQL mutation to update existing PetType
        graphQlTester.documentName("updatePetType")
                .variable("input", input)
                .executeAndVerify();

        //then: check PetType is updated correctly
        PetType updatedPetType = petTypeRepository.findById(petType.getId()).orElseThrow();

        //Check entity attributes
        assertThat(updatedPetType.getName()).isEqualTo("updatedName");
    }

    @Test
    public void testUpdateNonExistingPetType() {
        //given: initial data
        PetTypeInputDTO input = new PetTypeInputDTO();
        input.setId(Long.MAX_VALUE);

        //when: send a GraphQL mutation to update non-existing PetType
        //then: check an error in the response
        graphQlTester.documentName("updatePetType")
                .variable("input", input)
                .execute()
                .errors()
                .satisfy(responseErrors -> {
                    assertThat(responseErrors).hasSize(1);
                    assertThat(responseErrors.get(0).getErrorType()).isEqualTo(ErrorType.INTERNAL_ERROR);
                });
    }

    @Test
    public void testUpdateManyPetType() {
        //given: initial data
        PetType petType = createPetTypeEntity();
        petTypeRepository.saveAndFlush(petType);

        PetTypeInputDTO input = new PetTypeInputDTO();
        input.setId(petType.getId());
        input.setName("updatedName");

        List<PetTypeInputDTO> inputList = List.of(input);

        //when: send a GraphQL mutation to update existing PetTypes
        graphQlTester.documentName("updateManyPetType")
                .variable("inputList", inputList)
                .executeAndVerify();

        //then: check PetType is updated correctly
        PetType updatedPetType = petTypeRepository.findById(petType.getId()).orElseThrow();

        //Check entity attributes
        assertThat(updatedPetType.getName()).isEqualTo("updatedName");
    }

    private PetType createPetTypeEntity() {
        PetType petType = new PetType();
        petType.setName("name");

        return petType;
    }
}

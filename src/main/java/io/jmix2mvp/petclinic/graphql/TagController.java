package io.jmix2mvp.petclinic.graphql;

import com.amplicode.core.graphql.annotation.GraphQLId;
import io.jmix2mvp.petclinic.dto.TagDTO;
import io.jmix2mvp.petclinic.dto.TagInputDTO;
import io.jmix2mvp.petclinic.entity.Tag;
import io.jmix2mvp.petclinic.mapper.DTOMapper;
import io.jmix2mvp.petclinic.repository.TagRepository;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static io.jmix2mvp.petclinic.Authorities.ADMIN;
import static io.jmix2mvp.petclinic.Authorities.VETERINARIAN;

@Controller
public class TagController {
    private final TagRepository crudRepository;
    private final DTOMapper mapper;

    public TagController(TagRepository crudRepository, DTOMapper mapper) {
        this.crudRepository = crudRepository;
        this.mapper = mapper;
    }

    @MutationMapping(name = "deleteTag")
    @Transactional
    @Secured({ADMIN, VETERINARIAN})
    public void delete(@GraphQLId @Argument Long id) {
        Tag entity = crudRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Unable to find entity by id: %s ", id)));

        crudRepository.delete(entity);
    }

    @QueryMapping(name = "tagList")
    @Transactional
    @Secured({ADMIN, VETERINARIAN})
    public List<TagDTO> findAll() {
        return crudRepository.findAll().stream()
                .map(mapper::tagToTagDTO)
                .collect(Collectors.toList());
    }

    @QueryMapping(name = "tag")
    @Transactional
    @Secured({ADMIN, VETERINARIAN})
    public TagDTO findById(@GraphQLId @Argument Long id) {
        return crudRepository.findById(id)
                .map(mapper::tagToTagDTO)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Unable to find entity by id: %s ", id)));
    }

    @MutationMapping(name = "updateTag")
    @Transactional
    @Secured({ADMIN, VETERINARIAN})
    public TagDTO update(@Argument TagInputDTO input) {
        if (input.getId() != null) {
            if (!crudRepository.existsById(input.getId())) {
                throw new ResourceNotFoundException(
                        String.format("Unable to find entity by id: %s ", input.getId()));
            }
        }
        Tag entity = new Tag();
        mapper.tagDTOToEntity(input, entity);
        entity = crudRepository.save(entity);
        return mapper.tagToTagDTO(entity);
    }
}
package io.jmix2mvp.petclinic.repository;

import io.jmix2mvp.petclinic.dto.OwnerFilter;
import io.jmix2mvp.petclinic.entity.Owner;
import io.jmix2mvp.petclinic.entity.Owner_;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.Optional;

public class OwnerSpecifications {

    public static Specification<Owner> byFirstNameContains(String firstName) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.like(root.get("firstName"), "%" + firstName + "%");
    }

    public static Specification<Owner> byLastNameContains(String lastName) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.like(root.get("lastName"), "%" + lastName + "%");
    }

    public static Specification<Owner> byNamesStartsWith(OwnerFilter filter) {
        return Specification.where((root, query, criteriaBuilder) -> {
            if (filter == null) {
                return null;
            }

            Predicate firstNamePredicate = Optional.ofNullable(filter.getFirstName())
                    .map(value -> criteriaBuilder.like(
                            criteriaBuilder.lower(root.get(Owner_.firstName)),
                            value.toLowerCase() + "%"))
                    .orElse(criteriaBuilder.isTrue(criteriaBuilder.literal(true)));

            Predicate lastNamePredicate = Optional.ofNullable(filter.getLastName())
                    .map(value -> criteriaBuilder.like(
                            criteriaBuilder.lower(root.get(Owner_.lastName)),
                            value.toLowerCase() + "%"))
                    .orElse(criteriaBuilder.isTrue(criteriaBuilder.literal(true)));

            return criteriaBuilder.and(firstNamePredicate, lastNamePredicate);
        });
    }
}

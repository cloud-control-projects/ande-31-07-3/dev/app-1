package io.jmix2mvp.petclinic.repository;

import io.jmix2mvp.petclinic.entity.ScalarsTestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ScalarsTestEntityRepository extends JpaRepository<ScalarsTestEntity, Long>, JpaSpecificationExecutor<ScalarsTestEntity> {
}